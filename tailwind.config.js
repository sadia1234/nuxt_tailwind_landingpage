const colors = require('tailwindcss/colors')

module.exports = {
  purge: [],
  darkMode: 'media', // or 'media' or 'class'
  theme: {
    extend:{
    colors: {
      
        indigo: {
          light: "var(--light)",
          dark: "var(--dark)",
        },
        backdiv: {
          // primary:'#fff',
          // secondary:'#d1d5db',
          // turnary:'#6b7280',
          // primary:'#0D2438',
          // secondary:'#102C44',
          // turnary:'#1E3951'
          primary:"var(--bg-backdiv-primary)",
          secondary:"var(--bg-backdiv-secondary)",
          turnary:"var(--bg-backdiv-turnary)"
          
        },
        txtcolor: {
          txtlight:"var(--txtlight)",
        },
        btnhover: {
          btnfocus:"var(--btnfocus)",
        }
      },
      boxShadow: {
       
        DEFAULT: '0px 0px 50px -12px rgb(75,85,99)',
        
      },
      backgroundSize: {
        'auto': 'auto',
        'cover': 'cover',
        'contain': 'contain',
      
       '16': '10rem',
     
      }
      
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
  
}
